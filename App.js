/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {createStackNavigator, createAppContainer} from 'react-navigation';
import BuatPermohonan from './App/Permohonan/BuatPermohonan';
import RincianPermohonan from './App/Permohonan/RincianPermohonan';
import Home from './App/Home/Home';
import List from'./App/List/List';
import Profile from './App/Profile/Profile';
import Stok from './App/Stok/Stok';
import Perubahan from './App/Perubahan/Perubahan';


class App extends Component {
  render() {
    return <AppContainer />;
  }
}

const MainNavigator = createStackNavigator(
  {
    Home: { screen: Home },
    BuatPermohonan: { screen: BuatPermohonan },
    RincianPermohonan: { screen: RincianPermohonan },
    List: { screen: List },
    Profile: { screen: Profile },
    Stok: { screen: Stok },
    Perubahan: { screen: Perubahan }
  },{
    headerMode: 'none'
  }
);

const AppContainer = createAppContainer(MainNavigator);

export default App;