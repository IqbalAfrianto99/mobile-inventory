import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import HeaderHome from "../Components/HeaderHome";

class Perubahan extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#388bf2" }}>
        <HeaderHome
          onPress={() => this.props.navigation.navigate("List")}
          bgcolor="#388bf2"
          color="#ffffff"
          image={require("../../Assets/icon/round_close_24_px.png")}
          desc="Konfirmasi Perubahan"
        />
        <Text style={styles.contentTxt}>PARASETAMOL 500</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentTxt: {
    width: 227,
    height: 19,
    fontFamily: "Roboto",
    fontSize: 20,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "#ffffff",
    top: 24,
    left: 16
  }
});

export default Perubahan;
