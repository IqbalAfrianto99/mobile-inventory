import React, { Component } from "react";
import { StyleSheet, Image, View, Text } from "react-native";
import {
  Container,
  Content,
  List,
  ListItem,
  Left,
  Right,
  Button,
  Body,
  Card,
  CardItem,
  Input,
  Footer
} from "native-base";
import { Col, Grid } from "react-native-easy-grid";


export default class PermohonanList extends Component {
  render() {
    const { color, isRincian } = this.props;
    let rincian;
    
    if(isRincian){
        rincian = <Text style={ styles.listTitle }>Detail Barang</Text>
    }

    return (
        //{ rincian }
        <ListItem style={styles.listItem}>
          <Grid style={{ marginTop: 10 }}>
            <Col size={60}>
              <View style={{ marginTop: 15 }}>
                <Text style={styles.itemNameText}>PARASETAMOL 500</Text>
                <Text style={styles.itemTypeText}>Biji</Text>
              </View>
            </Col>

            <Col size={40} style={{ marginRight: 30 }}>
              <View>
                <Card style={styles.amountCard}>
                  <CardItem>
                    <Left style={{ right: 10 }}>
                      <Image
                        style={styles.imgIcon}
                        source={require("../../Assets/icon/icon_remove_rounded.png")}
                      />
                    </Left>
                    <Body style={{ marginRight: 17 }}>
                      <Input
                        style={styles.amountInput}
                        keyboardType="numeric"
                      />
                    </Body>
                    <Right style={{ marginLeft: 10, left: 10 }}>
                      <Image
                        style={styles.imgIcon}
                        source={require("../../Assets/icon/icon_add_rounded_green.png")}
                      />
                    </Right>
                  </CardItem>
                </Card>
                <View style={{ marginTop: 5, marginBottom: 5 }}>
                  <Text style={styles.stockText}>
                    Stok Gudang: <Text style={styles.stockAmount}>4500</Text>
                  </Text>
                </View>
              </View>
            </Col>
          </Grid>
        </ListItem>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    width: 360,
    height: 90,
    paddingTop: 10
  },
  listTitle: {
    marginTop: 8,
    marginLeft: 16,
    width: 98,
    height: 19,
    fontFamily: "Roboto",
    fontSize: 16,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 19,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.87)"
  },
  itemNameText: {
    width: 144,
    height: 19,
    fontFamily: "Roboto",
    fontSize: 16,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 19,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.87)"
  },
  itemTypeText: {
    marginTop: 5,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 0.24,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.54)"
  },
  imgIcon: {
    width: 24,
    height: 24
  },
  amountCard: {
    width: 135,
    height: 38,
    borderRadius: 4,
    backgroundColor: "#ffffff"
  },
  amountInput: {
    paddingBottom: 2,
    paddingTop: 2,
    fontSize: 14,
    fontWeight: "500",
    textAlign: "center",
    width: 55,
    height: 35,
    borderRadius: 2,
    backgroundColor: "rgba(0, 0, 0, 0.08)"
  },
  stockText: {
    marginTop: 5,
    fontFamily: "Roboto",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 14,
    letterSpacing: 0.4,
    textAlign: "right",
    color: "rgba(0, 0, 0, 0.87)"
  },
  stockAmount: {
    fontFamily: "Roboto",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 14,
    letterSpacing: 0.4,
    textAlign: "right",
    color: "rgba(0, 0, 0, 0.34)"
  }
});
