import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { Card, CardItem } from "native-base";

class DetailBarangCard extends Component {
  render() {
    const {
      status,
      barang,
      satuan,
      permohonan,
      pengambilan,
      diterima
    } = this.props;
    return (
      <View>
        <Card transparent style={styles.cardItem}>
          <Text style={styles.jenisTxt}>{barang}</Text>
          <Text style={styles.satuanTxt}>{satuan}</Text>

          <View
            style={{
              borderBottomColor: "rgba(0, 0, 0, 0.12)",
              borderBottomWidth: 1,
              top: 50
            }}
          />

          <CardItem
            style={{
              flexDirection: "row",
              top: 50,
              alignItems: "center",
              justifyContent: "space-between"
            }}
          >
            <View style={{ flex: 1, alignItems: "center" }}>
              {permohonan === 0 ? (
                <Text style={styles.jumlahNumber}>-</Text>
              ) : (
                <Text style={styles.jumlahNumber}>{permohonan}</Text>
              )}
              <Text style={styles.ketTxt}>Permohonan</Text>
            </View>

            <View style={{ flex: 1, alignItems: "center" }}>
              {pengambilan === 0 ? (
                <Text style={styles.jumlahNumber}>-</Text>
              ) : (
                <Text style={styles.jumlahNumber}>{pengambilan}</Text>
              )}
              <Text style={styles.ketTxt}>Pengambilan</Text>
            </View>

            <View style={{ flex: 1, alignItems: "center" }}>
              {diterima === 0 ? (
                <Text style={styles.jumlahNumber}>-</Text>
              ) : (
                <Text style={styles.jumlahNumber}>{diterima}</Text>
              )}
              <Text style={styles.ketTxt}>Diterima</Text>
            </View>
          </CardItem>

          <View
            style={{
              borderBottomColor: "rgba(0, 0, 0, 0.12)",
              borderBottomWidth: 1,
              top: 60
            }}
          />

          <CardItem
            style={{
              flexDirection: "row",
              top: 60,
              alignItems: "center",
              justifyContent: "space-between"
            }}
          >
            <View style={{ flex: 1, alignItems: "center" }}>
              {permohonan != 0 && status != "Meminta Barang" ? (
                <Image
                  small
                  source={require("../../Assets/icon/round_check_circle_outline_24_px(2).png")}
                  style={styles.checkImg}
                />
              ) : permohonan != 0 ? (
                <Image
                  small
                  source={require("../../Assets/icon/round_check_circle_outline_24_px.png")}
                  style={styles.checkImg}
                />
              ) : (
                <Image
                  small
                  source={require("../../Assets/icon/round_check_circle_outline_24_px(1).png")}
                  style={styles.checkImg}
                />
              )}
            </View>
            <View style={{ flex: 1, alignItems: "center" }}>
              {pengambilan != 0 && diterima != 0 ? (
                <Image
                  small
                  source={require("../../Assets/icon/round_check_circle_outline_24_px(2).png")}
                  style={styles.checkImg}
                />
              ) : pengambilan != 0 ? (
                <Image
                  small
                  source={require("../../Assets/icon/round_check_circle_outline_24_px.png")}
                  style={styles.checkImg}
                />
              ) : (
                <Image
                  small
                  source={require("../../Assets/icon/round_check_circle_outline_24_px(1).png")}
                  style={styles.checkImg}
                />
              )}
            </View>
            <View style={{ flex: 1, alignItems: "center" }}>
              {diterima != 0 ? (
                <Image
                  small
                  source={require("../../Assets/icon/round_check_circle_outline_24_px.png")}
                  style={styles.checkImg}
                />
              ) : (
                <Image
                  small
                  source={require("../../Assets/icon/round_check_circle_outline_24_px(1).png")}
                  style={styles.checkImg}
                />
              )}
            </View>
          </CardItem>

          {diterima != 0 ? (
            <Text style={styles.konfirmasiTxt}>SUDAH DITERIMA</Text>
          ) : status === "Barang Lengkap" ? (
            <TouchableOpacity
              style={styles.borderButton}
              onPress={() => this.props.navigation.navigate("Perubahan")}
            >
              <Text style={styles.textButton}>UBAH JUMLAH DITERIMA</Text>
            </TouchableOpacity>
          ) : null}
        </Card>

        <View style={styles.rectangle} />

        {diterima != 0 ? (
          <View style={styles.btnselesai}>
            <Text style={styles.txtselesai}>SELESAI</Text>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardItem: {
    width: 312,
    height: 284,
    borderRadius: 8,
    backgroundColor: "#ffffff",
    top: 68,
    left: 16
  },

  jenisTxt: {
    width: 200,
    height: 19,
    fontFamily: "Roboto",
    fontSize: 16,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 19,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.87)",
    left: 16,
    top: 37
  },

  satuanTxt: {
    width: 40,
    height: 16,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 0.24,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.54)",
    left: 16,
    top: 37
  },

  jumlahNumber: {
    width: 80,
    height: 40,
    fontFamily: "Roboto",
    fontSize: 34,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 40,
    letterSpacing: 0.24,
    textAlign: "center",
    color: "rgba(0, 0, 0, 0.87)"
  },

  ketTxt: {
    width: 80,
    height: 14,
    fontFamily: "Roboto",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 14,
    letterSpacing: 0.4,
    textAlign: "center",
    color: "rgba(0, 0, 0, 0.34)"
  },

  checkImg: {
    width: 24,
    height: 24,
    opacity: 1
  },

  borderButton: {
    width: 296,
    height: 48,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#ff7062",
    left: 8,
    top: 64
  },

  textButton: {
    width: 210,
    height: 16,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 1.25,
    textAlign: "center",
    color: "#ff7062",
    left: 50,
    top: 16
  },

  konfirmasiTxt: {
    width: 160,
    height: 16,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 1.25,
    textAlign: "center",
    color: "#388bf2",
    left: 80,
    top: 80
  },

  rectangle: {
    width: 24,
    height: 4,
    borderRadius: 2,
    backgroundColor: "#2accc7",
    top: 76,
    left: 168
  },

  btnselesai: {
    width: 313,
    height: 48,
    borderRadius: 4,
    backgroundColor: "#ff7062",
    left: 17,
    top: 88
  },

  txtselesai: {
    width: 70,
    height: 16,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 1.25,
    textAlign: "center",
    color: "#ffffff",
    left: 126,
    top: 15
  }
});

export default DetailBarangCard;
