import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Card, CardItem } from "native-base";

class StatusCard extends Component {
  render() {
    const { status, date } = this.props;
    return (
      <Card transparent style={styles.cardItem}>
        <Text style={styles.statusTxt}>{status}</Text>
        <Text style={styles.tanggalTxt}>{date}</Text>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  cardItem: {
    top: 28,
    left: 16,
    width: 240,
    height: 56,
    borderRadius: 4,
    borderColor: "#25b8b3",
    backgroundColor: "#25b8b3"
  },
  statusTxt: {
    width: 150,
    height: 19,
    fontFamily: "Roboto",
    fontSize: 16,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 19,
    letterSpacing: 0.49,
    textAlign: "left",
    color: "#ffffff",
    left: 16,
    top: 10
  },
  tanggalTxt: {
    width: 159,
    height: 14,
    fontFamily: "Roboto",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 14,
    letterSpacing: 0.4,
    textAlign: "left",
    color: "rgba(255, 255, 255, 0.5)",
    left: 16,
    top: 14
  }
});

export default StatusCard;
