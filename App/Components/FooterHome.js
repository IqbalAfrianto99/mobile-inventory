import React, { Component } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import { Footer } from "native-base";

class FooterHome extends Component {
  render() {
    return (
      <Footer style={styles.footer}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("Home")}
          style={{
            flex: 1
          }}
        >
          <Image
            small
            source={require("../../Assets/icon/round_home_24_px.png")}
            style={{ width: 24, height: 24, left: 48 }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("List")}
          style={{
            flex: 1
          }}
        >
          <Image
            small
            source={require("../../Assets/icon/round_list_alt_24_px.png")}
            style={{ width: 24, height: 24, left: 48 }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("List")}
          style={{
            flex: 1
          }}
        >
          <Image
            small
            source={require("../../Assets/icon/icon_circle_filled.png")}
            style={{ width: 24, height: 24, left: 48 }}
          />
        </TouchableOpacity>
      </Footer>
    );
  }
}

const styles = StyleSheet.create({
  footer: {
    width: 360,
    height: 56,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: "#ffffff",
    borderTopColor: "rgba(0, 0, 0, 0.12)",
    borderTopWidth: 1
  }
});

export default FooterHome;
