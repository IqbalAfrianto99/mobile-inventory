import React, { Component } from "react";
import { StyleSheet, Image, View, Text } from "react-native";
import { Right, Card, Button, CardItem } from "native-base";

import { Col, Grid } from "react-native-easy-grid";

export default class RincianFooter extends Component {
  render() {
    return (
      <Card style={styles.cardFooter} transparent>
        <Grid>
          <Col style={100}>
            <View style={{ marginLeft: 16, marginTop: 16, marginBottom: 16 }}>
              <Text style={styles.cardFooterText}>PESAN BARANG</Text>
            </View>
          </Col>
        </Grid>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  cardFooter: {
    marginRight: 16,
    marginLeft: 16,
    marginTop: 16,
    marginBottom: 16,
    width: 328,
    height: 48,
    borderRadius: 4,
    backgroundColor: "#ff7062"
  },
  cardFooterText: {
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 1.25,
    textAlign: "center",
    color: "#ffffff"
  }
});
