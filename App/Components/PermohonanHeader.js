import React, { Component } from "react";
import { StyleSheet, Image, View, TouchableOpacity } from "react-native";
import {
  Left,
  Right,
  Header,
  Title,
  Body
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import { withNavigation } from 'react-navigation';

class PermohonanHeader extends Component {
    render() {
        return(
            <Header transparent style={ styles.header }>
                <Left>
                    <TouchableOpacity onPress={ () => { this.props.navigation.goBack() }}>
                        <Image
                            style={styles.imgIcon}
                            source={require("../../Assets/icon/round_arrow_back.png")}
                        onPress={ () => { console.log('click') } }/>
                    </TouchableOpacity>
                </Left>
                <Body>
                    <Title style={styles.headerText}>{ this.props.title }</Title>
                </Body>
                <Right>
                    <Image
                        style={styles.imgAdd}
                        source={require("../../Assets/icon/round_add_circle.png")}
                    />
                </Right>
            </Header>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: "#ffffff",
        marginBottom: 10
    },
    headerText: {
        width: 194,
        height: 24,
        fontFamily: "Roboto",
        fontSize: 20,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 24,
        letterSpacing: 0.26,
        textAlign: "left",
        color: "rgba(0, 0, 0, 0.87)"
    },
    imgIcon: {
        width: 24,
        height: 24
    },
    imgAdd: {
        width: 24,
        height: 24,
        paddingRight: 16
    }
});

export default withNavigation(PermohonanHeader);