import React, { Component } from "react";
import { Image, TouchableWithoutFeedback } from "react-native";
import { Header, Left, Body, Title } from "native-base";

class HeaderHome extends Component {
  render() {
    const { onPress, color, image, desc, bgcolor } = this.props;
    return (
      <Header transparent style={{ backgroundColor: `${bgcolor}` }}>
        <Left>
          <TouchableWithoutFeedback onPress={onPress}>
            <Image
              small
              source={image}
              style={{ width: 30, height: 30, left: 10 }}
            />
          </TouchableWithoutFeedback>
        </Left>

        <Body>
          <Title
            style={{
              color: `${color}`,
              fontFamily: "Roboto",
              fontSize: 18,
              fontWeight: "500",
              fontStyle: "normal",
              lineHeight: 20,
              letterSpacing: 0.26,
              textAlign: "left",
              right: 24
            }}
          >
            {desc}
          </Title>
        </Body>
      </Header>
    );
  }
}

export default HeaderHome;
