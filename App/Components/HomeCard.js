import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableNativeFeedback
} from "react-native";
import { Card, CardItem, Body } from "native-base";

class HomeCard extends Component {
  render() {
    const { color, gambar, text, desc, onPress } = this.props;
    return (
      <View>
        <Card noShadow style={styles.cardBox}>
          <TouchableNativeFeedback onPress={onPress}>
            <CardItem
              style={{ width: 120, height: 120, backgroundColor: `${color}` }}
            >
              <Image small source={gambar} style={styles.thumbPng} />
              <Body>
                <Text style={styles.ketTxt}>{text}</Text>
                <Text style={styles.descTxt}>{desc}</Text>
              </Body>
            </CardItem>
          </TouchableNativeFeedback>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardBox: {
    width: 344,
    alignSelf: "center",
    height: 120,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    borderStyle: "solid",
    borderWidth: 2,
    borderColor: "rgba(0, 0, 0, 0.12)",
    marginTop: 12
  },
  thumbPng: {
    width: 36,
    height: 36,
    left: 25
  },
  ketTxt: {
    left: 84,
    marginTop: 8,
    width: 200,
    height: 28,
    fontFamily: "Roboto",
    fontSize: 23,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 28,
    letterSpacing: 0,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.7)"
  },
  descTxt: {
    left: 84,
    marginTop: 4,
    width: 178,
    height: 40,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0.25,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.4)"
  }
});

export default HomeCard;
