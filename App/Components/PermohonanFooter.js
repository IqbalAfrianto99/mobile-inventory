import React, { Component } from "react";
import { StyleSheet, Image, View, Text } from "react-native";
import {
  Right,
  Card,
  Button
} from "native-base";
import { Col, Grid } from "react-native-easy-grid";
import { withNavigation } from 'react-navigation';

class PermohonanFooter extends Component {

    constructor(props) {
        super(props);
    }


    navigateRincianPermohonan = () => {
        console.log('Clicked');
        this.props.navigation.navigate('RincianPermohonan');
    }

    render() {
        return(
            <Button style={styles.cardFooter} transparent onPress={ this.navigateRincianPermohonan }>
                <Grid>
                    <Col style={90}>
                        <View style={{ marginLeft: 16, marginTop: 5, marginBottom: 5 }}>
                            <Text style={styles.cardFooterText}>1 Barang</Text>
                            <Text style={styles.cardFooterTextMuted}>Dipilih</Text>
                        </View>
                    </Col>
                    <Col style={10}>
                        <Right style={{ marginLeft: 120 }}>
                            <Image style={styles.imgShop} source={require("../../Assets/icon/round_shopping_basket.png")} />
                        </Right>
                    </Col>
                </Grid>
            </Button>
        );
    }

}

const styles = StyleSheet.create({
    cardFooter: {
        marginTop: 16,
        marginRight: 16,
        marginLeft: 16,
        marginBottom: 16,
        width: 328,
        height: 48,
        borderRadius: 4,
        backgroundColor: "#ff7062"
    },
    cardFooterText: {
        fontFamily: "Roboto",
        fontSize: 14,
        fontWeight: "500",
        fontStyle: "normal",
        lineHeight: 16,
        letterSpacing: 0.24,
        textAlign: "left",
        color: "#ffffff"
    },
    cardFooterTextMuted: {
        marginTop: 3,
        fontFamily: "Roboto",
        fontSize: 12,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 14,
        letterSpacing: 0.24,
        textAlign: "left",
        color: "#ffffff"
    },
    footer: {
        width: 360,
        height: 80,
        backgroundColor: "#ffffff"
    },
    imgShop: {
        width: 24,
        height: 24,
        marginTop: 12,
        marginRight: 12
    }
});

export default withNavigation(PermohonanFooter);