import React, { Component } from "react";
import { StyleSheet, Text, Button, View } from "react-native";
import { Container, Body } from "native-base";

import HomeCard from "../Components/HomeCard";
import HeaderHome from "../Components/HeaderHome";
import FooterHome from "../Components/FooterHome";

class Home extends Component {
  render() {
    return (
      <Container>
        <HeaderHome
          onPress={() => this.props.navigation.navigate("Home")}
          bgcolor="#ffffff"
          color="rgba(0, 0, 0, 0.87)"
          image={require("../../Assets/icon/MaskGroup1.png")}
          desc="Mobile Tracking Inventory"
        />
        <View
          style={{
            borderBottomColor: "rgba(0, 0, 0, 0.12)",
            borderBottomWidth: 1
          }}
        />
        <Text style={styles.contentTxt}>Halo Kamu, lagi butuh apa?</Text>
        <Body>
          <HomeCard
            color="#2accc7"
            gambar={require("../../Assets/icon/icon_search_rounded.png")}
            text="Cek Stok"
            desc="Periksa persediaan stok barang ruangan"
            onPress={() => this.props.navigation.navigate("Stok")}
          />

          <HomeCard
            color="#ff7062"
            gambar={require("../../Assets/icon/icon_add_rounded.png")}
            text="Buat Permohonan"
            desc="Minta stok barang langsung ke gudang"
            onPress={() => this.props.navigation.navigate("BuatPermohonan")}
          />
        </Body>
        <FooterHome navigation={this.props.navigation} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerTxt: {
    color: "rgba(0, 0, 0, 0.87)",
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0.26,
    textAlign: "left",
    right: 24
  },
  contentTxt: {
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: 0.26,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.87)",
    left: 24,
    marginTop: 56,
    marginBottom: 12
  }
});

export default Home;
