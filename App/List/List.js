import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import HeaderHome from "../Components/HeaderHome";
import StatusCard from "../Components/StatusCard";
import DetailBarangCard from "../Components/DetailBarangCard";

class List extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#22a39f" }}>
        <HeaderHome
          onPress={() => this.props.navigation.navigate("Home")}
          bgcolor="#22a39f"
          color="#ffffff"
          image={require("../../Assets/icon/round_close_24_px.png")}
          desc="Rincian Permohonan"
        />
        <Text style={styles.contentTxt}>Status Permohonan</Text>
        <StatusCard status="Barang Lengkap" date="Rabu, 10 apr, 08:00" />

        <Text style={styles.detailBarangTxt}>Detail Barang</Text>
        <DetailBarangCard
          status="Barang Lengkap"
          barang="PARASETAMOL 500"
          satuan="Biji"
          permohonan={500}
          pengambilan={0}
          diterima={0}
          navigation={this.props.navigation}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentTxt: {
    width: 210,
    height: 24,
    fontFamily: "Roboto",
    fontSize: 20,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: 0.26,
    textAlign: "left",
    color: "#ffffff",
    top: 24,
    left: 16
  },
  detailBarangTxt: {
    width: 153,
    height: 24,
    fontFamily: "Roboto",
    fontSize: 20,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: 0.26,
    textAlign: "left",
    color: "#ffffff",
    top: 60,
    left: 16
  }
});

export default List;
