import React, { Component } from "react";
import { StyleSheet, Text, Button, View } from "react-native";
import HeaderHome from "../Components/HeaderHome";

class Stok extends Component {
  render() {
    return (
      <View>
        <HeaderHome
          onPress={() => this.props.navigation.navigate("Home")}
          bgcolor="#ffffff"
          color="rgba(0, 0, 0, 0.87)"
          image={require("../../Assets/icon/MaskGroup1.png")}
          desc="Mobile Tracking Inventory"
        />
      </View>
    );
  }
}
export default Stok;
