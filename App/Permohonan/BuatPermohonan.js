/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, Image, View, Text } from "react-native";
import {
  Container,
  Content,
  List,
  ListItem,
  Left,
  Right,
  Button,
  Body,
  Card,
  CardItem,
  Input,
  Footer
} from "native-base";
import { Col, Grid } from "react-native-easy-grid";

import PermohonanFooter from '../Components/PermohonanFooter';
import PermohonanHeader from '../Components/PermohonanHeader';
import PermohonanList from '../Components/PermohonanList';

export default class BuatPermohonan extends Component {

  render() {
    return (
      <Container style={styles.container}>
        <PermohonanHeader title="Permohonan Stok"/>
        <Content style={{ backgroundColor: "#ffffff" }}>
        <List style={{  marginBottom: 10 }}>
          <PermohonanList/>
        </List>
        </Content>
        <Footer style={styles.footer}>
          <PermohonanFooter navigation={ this.props.navigation }/>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f1f1f1"
  },
  footer: {
    width: 360,
    height: 80,
    backgroundColor: "#ffffff",
    borderTopWidth: 0.7,
    borderTopColor: "#f1f1f1"
  }
});
