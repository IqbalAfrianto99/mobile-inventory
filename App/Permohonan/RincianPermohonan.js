/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, Image, View, Text } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Container, Content, List } from "native-base";

import RincianFooter from "../Components/RincianFooter";
import PermohonanHeader from "../Components/PermohonanHeader";
import PermohonanList from "../Components/PermohonanList";

export default class BuatPermohonan extends Component {
  render() {
    return (
      <Container style={styles.container}>
        <PermohonanHeader title="Rincian Permohonan" />
         <Content>
            <List style={{ backgroundColor: "#ffffff", marginBottom: 10 }}>
              {(rincian = <Text style={styles.listTitle}>Detail Barang</Text>)}
              <PermohonanList />
              <PermohonanList />
              <PermohonanList />
            </List>
            <View style={{ marginTop: '50%' , bottom: 0, flex: 1 }}>
              <View>
                <Text style={styles.textBottom}>
                  Mohon cek kembali jumlah barang permohonan Anda apa sudah benar.
                </Text>
              </View>
            </View>
         </Content>
              <View style={styles.footer}>
                  <RincianFooter />
              </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f1f1f1",
    flex: 1
  },
  footer: {
    width: "100%",
    height: 80,
    backgroundColor: "#ffffff",
    borderTopWidth: 0.7,
    borderTopColor: "#f1f1f1"
  },
  textBottom: {
    marginRight: 20,
    marginLeft: 20,
    width: 320,
    height: 48,
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0.24,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.38)"
  },
  listTitle: {
    marginTop: 8,
    marginLeft: 16,
    width: 98,
    height: 19,
    fontFamily: "Roboto",
    fontSize: 16,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 19,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.87)"
  }
});
